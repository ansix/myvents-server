<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'facebook_id', 'avatar', 'notify_via_email', 'notify_via_push'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * The events that belong to the user.
     */
    public function events(){
        return $this->belongsToMany('App\Event');
    }


    /**
     * The ratings that belong to the user.
     */
    public function ratings(){
        return $this->hasMany('App\Rating');
    }
}

