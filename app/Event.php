<?php

namespace App;


use Auth;

use GuzzleHttp\Client;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

// http://veranstaltungen.kaernten.at/api/v2/events/RXZlbnREYi01NjAxNGRjMzZkNjU2NDNlZmM3MjAxMDA

class Event extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'startDate', 'endDate', 'url'
    ];

    /**
     * Loads Event from API
     */
    public function loadFromWebWithExternalId($id){
        $client = new Client();
        $res = $client->request('GET', "http://veranstaltungen.kaernten.at/api/v2/events/" . $id);

        return json_decode($res->getBody())->event[0];
    }

    /**
     * The users that belong to the event.
     */
    public function users(){
        return $this->belongsToMany('App\User');
    }

    /**
     * The ratings that belong to the event.
     */
    public function ratings(){
        return $this->hasMany('App\Rating');
    }

    /**
     * Is this Event rated by the authenticated user?
     *
     * @return boolean
     */
    public function isRatedByAuthenticatedUser(){
        return $this->ratings()->get()->isEmpty() ? false : !$this->ratings()->get()->where('user_id', Auth::user()->id)->isEmpty();
    }

    /**
     * Is this Event added by the authenticated user?
     *
     * @return boolean
     */
    public function isAddedByAuthenticatedUser(){
        return !$this->users()->get()->where('id', Auth::user()->id)->isEmpty();
    }

    /**
     * Get the event's rating.
     *
     * @return float
     */
    public function getAvgRating(){
        $ratings = $this->ratings()->get();

        return count($ratings) > 0 ? $ratings->pluck('rating')->sum() / count($ratings) : 0;
    }

    /**
     * Prepares and returns all internal Events
     *
     * @return collection of events
     */
    public static function prepareEvents(){
        $events =  Auth::user()->events()->get();

        foreach ($events as $event) {
            $event->rating = 0;
            $event->alreadyRated = false;
            $event->alreadyAdded = false;

            $event = $event->prepareEvent($event);
        }

        return $events;
    }

    /**
     * Prepare Event - calc avg. Rating set alreadyRated / alreadyAdded flags
     *
     * @param  \App\Event  $event
     * @return \App\Event  $event
     */
    public function prepareEvent(Event $event){
        // Check for rating and calculate rating
        $event->rating = $event->getAvgRating();

        // Check if user did rate event already
        if(Auth::user()){
            $event->alreadyRated = $event->isRatedByAuthenticatedUser();
            $event->alreadyAdded = $event->isAddedByAuthenticatedUser();
        }

        return $event;
    }


}
