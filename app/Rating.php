<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Rating extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ratings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'myevent_id', 'rating'
    ];

 	/**
     * Get the user that owns the rating.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

 	/**
     * Get the event that owns the rating.
     */
    public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
