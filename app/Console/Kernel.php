<?php

namespace App\Console;

use App\User;

use Mail;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(){
            $users = User::all();

            foreach ($users as $user) {
                $events = $user->events()->get();

                foreach ($events as $event) {
                    $beginSearch = \Carbon\Carbon::now();
                    $endSearch = \Carbon\Carbon::now();
                    $endSearch->addMinutes(60);

                    $needle = \Carbon\Carbon::createFromFormat('Y-m-d H:i:u', $event->startDate);

                    //die($beginSearch . " " . $endSearch . " " . $needle . " " . $event->startDate);

                    if($beginSearch->lte($needle) && $endSearch->gte($needle)){


                        Mail::send('emails.notification', ['data' => $user, 'event' => $event], function ($message) use ($user, $event) {
                            $message->to('gerdkollmann@me.com');
                        });
                    }  
                }

            }
        })->everyMinute();
    }
}
