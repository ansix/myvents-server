<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Event;
use App\Rating;

use GuzzleHttp\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PublicController extends Controller
{
    protected $filter;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->filter = array();
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $request->session()->put('filter', $request->filter);

        $client = new Client();

        $cat_result =$client->request('GET', 'http://veranstaltungen.kaernten.at/api/v2/categories');
        $categories = json_decode($cat_result->getBody())->categories;
        $cats = array();

        foreach ($categories as $catObj) {
            $cats[$catObj->id] = $catObj->name;    
        }

        $events = $this->prepareEvents($request->filter, 1);

        return view('public', [ 'events' => $events,
                                'categories' => $cats,
                                'filter' => $request->filter]);
    }


    /**
     * Load page as HTML or JSON
     * 
     * @param int       $page
     * @param Request   $request
     * @return JSON or HTML Response
     */
    public function loadPage($page, Request $request){
        $filter = $request->session()->pull('filter', $request->filter);

        $events = $this->prepareEvents($filter, $page);

        if($request->json){
            return response()->json($events);
        } else{
            return view('partials.events', ['events' => $events]);
        }
    }

    /**
     * Set Filter, gets Events, check if user already added / shared Event
     * 
     * @param array $filter
     * @param int   $page
     * @return collection of events
     */

    protected function prepareEvents($filter, $page){
        $filterString = "";
        
        if(isset($filter['category'])){
            $filterString .= "&filter[category]=" . $filter['category'];
        }
        if(isset($filter['from'])){
            $filterString .= "&filter[from]=" . $filter['from'];
        }
        if(isset($filter['to'])){
            $filterString .= "&filter[to]=" . $filter['to'];
        }

        $client = new Client();
        $res = $client->request('GET', 'http://veranstaltungen.kaernten.at/api/v2/endpoints/557ea81f6d6564769e010000.json?pagesize=10&page=' . $page . $filterString);

        $events =  json_decode($res->getBody())->events;

        foreach ($events as $event) {
            $event->rating = 0;
            $event->alreadyRated = false;
            $event->alreadyAdded = false;

            $savedEvent = Event::where('url', $event->url)->first();

            if($savedEvent){
                // Check for rating and calculate rating
                $event->rating = $savedEvent->getAvgRating();

                // Check if user did rate event already
                if(Auth::user()){
                    $event->alreadyRated = $savedEvent->isRatedByAuthenticatedUser();
                    $event->alreadyAdded = $savedEvent->isAddedByAuthenticatedUser();
                }
            }
        }

        return $events;
    }
}
