<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\User;
use App\Http\Requests;
use App\Http\Requests\SettingPostRequest;

class SettingsController extends Controller
{
    /**
     * The current user.
     */
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->user = Auth::user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.index', ['user' => Auth::user()]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SettingPostRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingPostRequest $request, $id)
    {
        var_dump($request->notify_via_email);
        $this->user->email = $request->email;
        $this->user->name = $request->name;
        $this->user->notify_via_email = $request->notify_via_email == "on" ? 1: 0;
        $this->user->notify_via_push = $request->notify_via_push == "on" ? 1 : 0;

        $this->user->save();

        return redirect('/');
    }

}
