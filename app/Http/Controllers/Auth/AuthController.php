<?php

namespace App\Http\Controllers\Auth;

use Socialite;
use Parse\ParseUser;
use Auth;
use Mail;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        Mail::send('emails.welcome', ['data' => $data], function ($message) use ($data) {
            $message->to($data['email'])->cc('gerdkollmann@me.com');
        });

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Redirect user to Facebook auth
     *
     * @param Response
     */
    public function redirectToProvider(){
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Get User data from Facebook
     *
     * @param Response
     */
    public function handleProviderCallback(){
        try {
            $user = Socialite::driver('facebook')->user();
        } catch(Exception $e){
            return redirect('auth/facebook');
        }

        $authenticatedUser = $this->findOrCreateUser($user);

        Auth::login($authenticatedUser, true);

        return redirect()->intended('/');
    }

    /**
     * Returns user if it exists - or creates and returns if not
     *
     * @param $fbUser
     * @return User
     */
    protected function findOrCreateUser($fbUser){
        $existingUser = User::where('facebook_id', $fbUser->id)->first();

        if($existingUser){
            return $existingUser;
        }

        return User::create([
            'name' => $fbUser->name,
            'email' => $fbUser->email,
            'facebook_id' => $fbUser->id,
            'avatar' => $fbUser->avatar
            ]);
    }
}
