<?php

namespace App\Http\Controllers;

use Auth;

use App\Event;
use App\Rating;

use Parse\ParseObject;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EventController extends Controller
{

    /**
     * The current user.
     */
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::prepareEvents();

        return view('myEvents', ['events' => $events]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $user = Auth::user();

        $event = $this->findOrCreateEvent($request);

        $event->name = $request->title;
        $event->url = $request->url;
        $event->eventdb_id = basename($event->url);

        $event->save();


        if(!$event->users()->where('user_id', $user->id)->first()){
            $event->users()->attach($user->id);
            $event->save();

            $this->storeInParse($event, $user);
        }
    }

    /**
     * Find or Create Event
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Event
     */
    private function findOrCreateEvent(Request $request){
        $existingEvent = Event::where('url', $request->url)->first();

        if($existingEvent){
            return $existingEvent;
        }
        
        $event = new Event;
        $eventData = $event->loadFromWebWithExternalId(basename($request->url));
        $event->name = $eventData->name;
        $event->startDate = $eventData->startDate;
        $event->endDate = $eventData->endDate;

        return $event;
    }

    /**
     * Detach Event from current User
     *
     * @param  \Illuminate\Http\Request $request
     * @return true
     */
    public function destroy(Request $request){
        $event = Event::where('url', $request->url)->first();

        $this->user->events()->detach($event->id);

        return json_encode(array('success' => true));
    }

    /**
     * Store Event in Parse
     * 
     * @param Event, User
     */

    private function storeInParse($event, $user){
        $parseEvent = new ParseObject("Event");

        $parseEvent->set("user", $user->id);
        $parseEvent->set("title", $event->name);

        try{
            $parseEvent->save();
        } catch(ParseException $e){
            die($e->getMessage);
        } 
    }

    /**
     * Rate an Event in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function rate(Request $request){
        $event = $this->findOrCreateEvent($request);

        $event->name = $request->title;
        $event->url = $request->url;
        $event->eventdb_id = basename($event->url);
        $event->save();

        $rating = new Rating;
        $rating->rating = $request->rating;

        $event->ratings()->save($rating);
        $this->user->ratings()->save($rating);

        $rating->save();

        return json_encode(array("success" => true));
    }

    /**
     * load modal dynamically for rating, share
     *
     * @param string $id
     * @param string $action
     * @return Response 
     **/
    public function loadModal($id, $action){
        $event = new Event;
        $event = $event->loadFromWebWithExternalId($id);
        $internal_event = Event::where('eventdb_id', $id)->first();


        /*** WRONG PLACE ... !
        if($internal_event && $internal_event->isAddedByAuthenticatedUser()){
            return view('partials.done', ['action_string' => "hinzugefügt"]);
        }
        */

        switch ($action) {
            case 'rate':
                if($internal_event && $internal_event->isRatedByAuthenticatedUser()){
                    return view('partials.done', ['action_string' => 'bewertet']);
                }
                return view('partials.rate', ['event' => $event]);
                break;
            
            case 'share':
                return view('partials.share', ['event' => $event]);
                break;

            case 'remove':
                return view('partials.remove', ['event' => $event]);
                break;

            default:
                # do nothing
                break;
        }
    }
}
