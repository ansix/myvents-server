<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SettingPostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:255',
            'email' => 'email|max:255',
            'notify_via_email' => 'max:2',
            'notify_via_push' => 'max:2'
        ];
    }
}
