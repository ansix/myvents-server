<?php

namespace App\Http\Middleware;

use Url;
use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                $request->session()->set('status', 'login');
                return redirect()->guest('login');
            } else {
                return redirect()->guest('login');
            }
        }


        if($request->session()->get('status') == "login" && strpos($request->path(), '/load-')){
            $request->session()->set('status', '');
            return redirect('/#'.$request->path());
        } else {
            return $next($request);
        }
    }
}
