@extends('layouts.app')

@section('content')

<div class="row">
	<div class="large-12 columns">
		{!! Form::model($user, array('route' => array('settings.update', $user->id), 'method' => 'PUT')) !!}
		<div class="row">
			<div class="large-2 columns">
				<img src="{{ Auth::user()->avatar }}" width="128" height="128">
			</div>
			<div class="large-10 columns">
				<div class="row">
					<div class="large-12 columns">
						{!! Form::text('name', $user->name); !!}
					</div>
					<div class="large-12 columns">
						{!! Form::text('email', $user->email); !!}
					</div>
					<div class="large-12 columns">
						<a href="{{ url('/password/reset') }}" class="button expanded secondary">Passwort zurücksetzen</a>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="large-1 large-offset-2 columns">
				<div class="switch">
				  <input class="switch-input" id="emailNotifications" type="checkbox" name="notify_via_email" {{ $user->notify_via_email == 1 ?  'checked' : '' }}>
				  <label class="switch-paddle" for="emailNotifications">
				    <span class="show-for-sr">schicke mir eine Email vor jedem Event.</span>
				  </label>
				</div>
			</div>
			<div class="large-9 columns">
				Schicke mir eine Email vor jedem Event.
			</div>
		</div>
		<div class="row">
			<div class="large-1 large-offset-2 columns">
				<div class="switch">
				  <input class="switch-input" id="pushNotifications" type="checkbox" name="notify_via_email" {{ $user->notify_via_push == 1 ? 'checked' : '' }}>
				  <label class="switch-paddle" for="pushNotifications">
				    <span class="show-for-sr">schicke mir eine Push Notification vor jedem Event.</span>
				  </label>
				</div>
			</div>
			<div class="large-9 columns">
				Schicke mir eine Push Notification vor jedem Event.
			</div>
		</div>
		<div class="row">
			<div class="large-10 large-offset-2 columns">
				<input type="submit" class="button" value="Speichern">
				<a href="{{ url('/') }}" class="button secondary">Abbrechen</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

@endsection