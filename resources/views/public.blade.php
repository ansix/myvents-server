@extends('layouts.app')

@section('content')

<div class="expanded row header" style="background: url({{ count($events) > 0 ? $events[0]->image->contentUrl : '#ddd' }}) center center;">
    <div class="large-5 large-centered columns">
        <form action="/">
            <div class="row large-collapse">
                <div class="large-3 columns">
                    <div class="input-group">
                        <span class="input-group-label"><i class="fa fa-lightbulb-o"></i></span>
                        {!! Form::select('filter[category]', $categories, $filter['category'], array('class' => 'input-group-field')) !!}
                    </div>
                </div>
                <div class="large-4 columns">
                    <div class="input-group">
                        <span class="input-group-label"><i class="fa fa-calendar-o"></i></span>
                        {!! Form::text('filter[from]', isset($filter['from']) ? $filter['from'] : \Carbon\Carbon::now()->format('d.m.Y'), array('class' => 'input-group-field')) !!}
                    </div>
                </div>
                <div class="large-5 columns">
                    <div class="input-group">
                        <span class="input-group-label">bis</span>
                        {!! Form::text('filter[to]', isset($filter['to']) ? $filter['to'] : \Carbon\Carbon::now()->format('d.m.Y'), array('class' => 'input-group-field')) !!}    
                        <div class="input-group-button">
                            <input type="submit" class="button" value="Suchen">
                        </div>
                    </div>           
                </div>
            </div>
        </form>
    </div>
    <div class="row">
    <div class="small-12 large-6 large-centered columns">
        <ul class="tabs" data-tabs id="event-tabs">
            <li class="tabs-title is-active">
                <a href="#event-list" aria-selected="true">Liste</a>
            </li>
            <li class="tabs-title">
                <a href="#event-map" >Karte</a>
            </li>
        </ul>
    </div>
    </div>
</div>
<div class="tabs-content" data-tabs-content="event-tabs">
    <div id="event-list" class="tabs-panel is-active">
        <div class="row">
            <div class="small-12 large-6 large-centered columns events">

                @each('partials.event', $events, 'event', 'partials.event_empty')

            </div>

        </div>

        @unless(count($events) < 10)
        <div class="row">
            <div class="small-12 large-8 large-centered columns">
                <a href="" class="button expanded load-more">lade mehr Events</a>
            </div>
        </div>
        @endunless
        
    </div>
    <div id="event-map" class="tabs-panel"></div>
</div>


@push('scripts')
<script>
    $(document).ready(function() {
        // SET CSFR TOKEN
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
        });

        // PAGING
        var page = 1;

        $(document).on('click', '.load-more', function(e){
            e.preventDefault();
            page++;
            $.ajax('/page/'+page)
                .done(function(resp){
                    $('.events').append(resp);
                    initMapWithCurrentPage();
                });
        });
        
                
        // MAPBOX CLUSTER
        var markers = new L.MarkerClusterGroup();

        var initMapWithCurrentPage = function(){
            map.removeLayer(markers);

            $.ajax({
                url: '/page/'+page,
                data: {'json': true}
            })
                .done(function(events){

                    $.each(events, function(index, val) {
                        if(val.location.geo){
                        var event = val;
                        var title = val.name;

                        var marker = L.marker(new L.latLng(val.location.geo.longitude, val.location.geo.latitude), {
                            icon: L.mapbox.marker.icon({
                                'marker-symbol': 'circle', 
                                'marker-color': '1b524f'}),
                            title: title
                        });
                        marker.bindPopup(title);
                        markers.addLayer(marker);
                        }
                    });

                    map.addLayer(markers);
                });
        }
        initMapWithCurrentPage();

        openModalOnLoad = function(){
            var link = location.hash.split("/"),
                url = '/events/' + link[1] + '/' + link[2];
            location.hash = "";


            if(link.length > 0){
                $.ajax(url)
                    .done(function(resp){
                        $modal.html(resp).foundation('open');
                    });
            }
        }
        
        openModalOnLoad();

        submitForm = function(data){
            $.ajax({
              url: 'events' + data.action,
              type: 'POST',
              dataType: 'json',
              data: data,
              success: function(data, textStatus, xhr) {
                $modal.foundation('close');
                location.reload();
              },
              error: function(xhr, textStatus, errorThrown) {
                console.log(xhr, textStatus, errorThrown);
                location.reload();
              }
            });
            
        }

        // LIVE EVENTS FOR PARTIALS
        $(document).on('click', '.login', function(e){
            $.ajax({
                url: '/login',
            })
                .done(function(resp){
                    console.log('asdf');
                    $modal.html(resp).foundation('open');
                });
            e.preventDefault();
        });

        $(document).on('click','.rate-event', function(event) {
            event.preventDefault();
            var data = $(this).data();
            $.extend(true, data, {"action": "/rate"});

            submitForm(data);
        });


        $(document).on('click', '.share-event', function(event) {
            event.preventDefault();
        });

    });
</script>
@endpush

@endsection
