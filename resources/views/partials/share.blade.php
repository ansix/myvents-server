<!-- TODO Implement Share code here  
SHARE CODES
{{ $event->url }}
-->

<div class="share-event-modal text-center">
	<div class="row">
		<div class="small-6 large-6 large-centered columns">
			<img src="images/logo_big.png" id="share-logo" />
		</div>
	</div>
	<div class="row">
		<div class="small-12 large-6 large-centered columns">
			<p>Teile das Event</p>
		</div>
	</div>
	
	<div class="row" >

		<!--Facebook Share-->
		<div class="small-4 large-4 columns">
			<a href="http://www.facebook.com/sharer/sharer.php?u={{ $event->url }}" target="_new">
			<i class="fa fa-3x fa-facebook"></i></a>
		</div>	

		<!--Google+ Share-->
		<div class="small-4 large-4 columns">
			<a href="https://plus.google.com/share?url={{ $event->url }}" target="_new">
			<i class="fa fa-3x fa-google-plus"></i></a>

		</div>

		<!--Twitter Share-->
		<a href="http://www.twitter.com/share?text={{ $event->name }}&amp;url={{ $event->url }}" class="animated-icon" target="_new"> <i class="fa fa-3x fa-twitter"></i></a>
	</div>
</div>
	
	
