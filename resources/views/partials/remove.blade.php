<div class="share-event-modal">
	<div class="row">
		<div class="small-6 large-6 large-centered columns">
			<img src="images/logo_big.png" id="share-logo" />
		</div>
	</div>
	<div class="row">
		<div class="small-12 large-6 large-centered columns">
			<p>Möchtes du das Event wirklich aus deiner Liste entfernen?</p>
		</div>
	</div>
	
	<div class="row">
		<a href="#" id="remove-button" class="button alert">
			Event entfernen!
		</a>
	</div>
</div>

<button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
</button>


<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
    });


	  $('#remove-button').on('click', function(e){
    e.preventDefault();

    $.ajax({
      url: 'events/<?php echo $event->id ?>',
      type: 'DELETE',
      dataType: 'json',
      data: {
        url: "<?php echo $event->url; ?>"
      },
    }).done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
        alert("Bewertung konnte nicht verarbeitet werden.");
      })
      .always(function() {
        console.log("complete");
        location.reload();
      });
    });
</script>
