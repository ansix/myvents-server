<div class="event row">
    <div class="row">
        <div class="small-2 large-1 columns">
            <div class="date-box">
                <div class="date-box-day">{{ Carbon\Carbon::parse($event->startDate)->format('d') }}</div>
                <div class="date-box-month">{{ Carbon\Carbon::parse($event->startDate)->format('M') }}</div>
                <div class="date-box-time">{{ Carbon\Carbon::parse($event->startDate)->format('h:i') }}</div>
            </div>
            @unless ($event->startDate == $event->endDate)
                <div class="date-box second">
                    <div class="date-box-day">{{ Carbon\Carbon::parse($event->endDate)->format('d') }}</div>
                    <div class="date-box-month">{{ Carbon\Carbon::parse($event->endDate)->format('M') }}</div>
                    <div class="date-box-time">{{ Carbon\Carbon::parse($event->startDate)->format('h:i') }}</div>                
                </div>
            @endunless
        </div>
        <div class="small-10 large-8 columns">
            <div class="row">
                <div class="large-12 columns">
                    <h3>{{ $event->name }}</h3> 

                </div>
            </div>
        </div>
        <div class="large-3 columns">
            <div class="actions">    
                @unless ($event->alreadyRated)
                    <a href="#" class="action rate-event-modal" 
                        data-action="rate" 
                        data-url="{{ $event->url }}"
                        data-tooltip
                        title="Bewerte dieses Event!">
                @endunless
                    @for ($i = 1; $i <= $event->getAvgRating(); $i++)
                        <i class="fa fa-star"></i>
                    @endfor


                    @for ($j = $event->getAvgRating()+1; $j <= 3; $j++)
                        <i class="fa fa-star-o"></i>
                    @endfor
                
                @unless ($event->alreadyRated)
                    </a>
                @endunless

                <a href="#" class="action delete-event-modal delete"
                    data-action="remove"
                    data-id="{{ $event->id }}"
                    data-url="{{ $event->url }}"
                    data-tooltip
                    title="Entferne dieses Event aus deiner Liste!">
                    <i class="fa fa-trash"></i>
                </a>
            
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-11 large-offset-1 columns">
            <div class="description">

            </div>
        </div>
    </div>

</div>                     