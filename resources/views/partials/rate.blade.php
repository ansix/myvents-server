

    <div class="row">
    	<div class="small-12 large-12 large-centered columns">
    		<div class="row" id="rate-modal">
    			<div class="small-12 large 12 large-centered columns text-center">
    				<img src="/images/logo_big.png" alt="Logo Myvents" />
        			<p>Bewerte das Event</p>
    			</div>
    		</div>

    	</div>
    </div>
    	<div class="row text-center" id="rating_stars">
    		<div class="small-12 large-12 large-centered columns">
    			<div class="small-4 large-4 columns">
          			<i class="fa fa-3x fa-star-o" id="star1"></i>
        		</div>
        		<div class="small-4 large-4 columns">
         		 <i class="fa fa-3x fa-star-o" id="star2"></i>
        		</div>
		        <div class="small-4 large-4 columns">
          			<i class="fa fa-3x fa-star-o" id="star3"></i>
        		</div>
        	<input type="hidden" name="rate" value="1">
    		</div>
    	</div> 
    	<div class="row">
    		<div class="small-12 large-12 large-centered columns text-center">
    		 <a href="#" class="button expand" id="rate-button">meine Bewertung abschicken</a>
    		</div>
    	</div>
        
        <script type="text/javascript">
          var starOne   =$("#star1");
          var starTwo   =$("#star2");
          var starThree =$("#star3");
          var rateVal   =$("input[name=rate]");

          starThree.on('click', function(e) {
            e.preventDefault();
            starOne.removeClass("fa-star-o").addClass("fa-star");
            starTwo.removeClass("fa-star-o").addClass("fa-star");
            starThree.removeClass("fa-star-o").addClass("fa-star");
            rateVal.val("3");
          });
          starTwo.on('click', function(e) {
            e.preventDefault();
            starOne.removeClass("fa-star-o").addClass("fa-star");
            starTwo.removeClass("fa-star-o").addClass("fa-star");
            starThree.removeClass("fa-star").addClass("fa-star-o");
            rateVal.val("2");
          });
          starOne.on('click', function(e) {
            e.preventDefault();
            starOne.removeClass("fa-star-o").addClass("fa-star");
            starTwo.removeClass("fa-star").addClass("fa-star-o");
            starThree.removeClass("fa-star").addClass("fa-star-o");
            rateVal.val("1");
          });

          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
          });

          $('#rate-button').on('click', function(e){
            e.preventDefault();

            $.ajax({
              url: 'events/rate',
              type: 'POST',
              dataType: 'json',
              data: {
                rating: rateVal.val(),
                title: "<?php echo $event->name; ?>",
                url: "<?php echo $event->url; ?>"
              },
            })
            .done(function() {
              console.log("success");
            })
            .fail(function() {
              console.log("error");
              alert("Bewertung konnte nicht verarbeitet werden.");
            })
            .always(function() {
              console.log("complete");
              location.reload();
            });
          });
        

        </script>
      <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
    



