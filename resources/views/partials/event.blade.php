<div class="event row">
    <div class="row">
        <div class="small-2 large-1 columns">
            <div class="date-box">
                <div class="date-box-day">{{ Carbon\Carbon::parse($event->startDate)->format('d') }}</div>
                <div class="date-box-month">{{ Carbon\Carbon::parse($event->startDate)->format('M') }}</div>
                <div class="date-box-time">{{ Carbon\Carbon::parse($event->startDate)->format('h:i') }}</div>
            </div>
            @unless ($event->startDate == $event->endDate)
                <div class="date-box second">
                    <div class="date-box-day">{{ Carbon\Carbon::parse($event->endDate)->format('d') }}</div>
                    <div class="date-box-month">{{ Carbon\Carbon::parse($event->endDate)->format('M') }}</div>
                    <div class="date-box-time">{{ Carbon\Carbon::parse($event->startDate)->format('h:i') }}</div>                
                </div>
            @endunless
        </div>
        <div class="small-10 large-11 columns">
            <div class="row">
                <div class="small-10 large-9 columns">
                    <div class="row">
                        <div class="large-12 columns">
                            <h3>{{ $event->name }}</h3> 

                        </div>
                    </div>
                </div>
                <div class="large-3 columns">
                    <div class="actions">              

                        @unless ($event->alreadyRated)
                            <a href="#" class="action rate-event-modal" 
                                data-action="rate" 
                                data-url="{{ $event->url }}"
                                data-tooltip
                                title="Bewerte dieses Event!">
                        @endunless
                            @for ($i = 1; $i <= $event->rating; $i++)
                                <i class="fa fa-star"></i>
                            @endfor


                            @for ($j = ++$event->rating; $j <= 3; $j++)
                                <i class="fa fa-star-o"></i>
                            @endfor
                        
                        @unless ($event->alreadyRated)
                            </a>
                        @endunless
                        
                        {!! Html::decode(link_to('#', '<i class="fa fa-share"></i>', array(
                            "class" => "action share-event-modal", 
                            "data-action" => "share", 
                            "data-title" => $event->name, 
                            "data-url" => $event->url,
                            "data-tooltip" => "",
                            "title" => "Teile dieses Event mit deinen Freunden!"
                            ), false)) !!}
                        
                        @unless ($event->alreadyAdded)
                            {!! Html::decode(link_to('#', '<i class="fa fa-heart"></i>', array(
                                "class" => "action add-event-modal", 
                                "data-action" => "add", 
                                "data-title" => $event->name, 
                                "data-url" => $event->url,
                                "data-tooltip" => "",
                                "title" => "Füge dieses Event zu deinen Events hinzu!"), 
                                false)) !!}
                        @endunless

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <div class="description">
                        {!! $event->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <div class="event-image-container" style="background: url({{ $event->image->contentUrl }}) center center;"></div>
        </div>
    </div>

</div>                     