<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MyVents</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
   

    <!-- Scripts -->
    <script src='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.js'></script>
    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>


    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet' />


</head>
<body id="app-layout">
    <div class="row expanded collapse" id="nav" data-sticky-container>
        <div class="large-12 columns" data-sticky data-options="marginTop:0;" style="width:100%">
            <div class="title-bar" data-responsive-toggle="main-nav" data-hide-for="medium">
                <button class="menu-icon" type="button" data-toggle><i class="fa fa-bars"></i></button>
                <div class="title-bar-title">                    
                    <a href="{{ url('/') }}">
                        {!! Html::image('/images/logo_header.png') !!}
                    </a>
                </div>
            </div>
            <div class="top-bar" id="main-nav">
                <div class="top-bar-title hide-for-small-only">
                    <a href="{{ url('/') }}">
                        {!! Html::image('/images/logo_header.png') !!}
                    </a>
                </div>
                <div class="top-bar-left">
                    <ul class="menu">
                        <li><a href="{{ url('/') }}"><span>Alle</span> Events</a></li>
                        <li><a href="{{ url('/events') }}"><span>Meine</span> Events</a></li>
                    </ul>
                </div>
                <div class="top-bar-right">
                    <ul class="dropdown menu" data-dropdown-menu>
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}" class="login">Login</a></li>
                            <li><a href="{{ url('/register') }}">Registriere Dich</a></li>
                        @else
                            <li>
                                <a href="#">
                                    <span>Willkommen</span> {{ Auth::user()->name }}
                                    <img src="{{ Auth::user()->avatar }}" width="50">

                                </a>
                                <ul class="menu vertical">
                                    <li><a href="{{ url('/events') }}">Meine Events</a></li>
                                    <li><a href="{{ url('/settings') }}">Einstellungen</a></li>
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                </ul>

                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>


    @yield('content')

    <footer class="expanded row">
        <div class="large-12 columns">
            <div class="menu-centered ">
                <ul class="menu">
                    <li><a href="">Über uns</a></li>
                    <li><a href="">Impressum</a></li>
                    <li>(c) 2016 Myvents.net</li>
                </ul>
            </div>
            
        </div>
    </footer>


    <div class="reveal" id="modal" data-reveal data-animation-in="slide-in-down" data-animation-out="slide-out-up" data-close-on-click="true"></div>

    <div id="map" style="display:none;"></div>
    <!-- JavaScripts -->
    <script src="/js/all.js"></script>
    <script>
        L.mapbox.accessToken = 'pk.eyJ1IjoiYW5zaXgiLCJhIjoiY2loeG94eDIyMDA5cXZxbTVybzBvbHlhaSJ9.h_2mtBn87MyxYeNZ8MWprA';
        var map = L.mapbox.map('event-map', 'mapbox.streets')
                    .setView([46.71, 13.96], 10);

        L.mapbox.styleLayer('mapbox://styles/ansix/cilghah90006nbbm5692z6qkw').addTo(map);
    </script>
    </script>
    @stack('scripts')
</body>
</html>
