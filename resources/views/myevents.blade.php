@extends('layouts.app')

@section('content')

		<div class="row">
			<div class="small-12 large-6 large-centered columns events">
				<h2>Meine Events</h2>
				<p class="lead">Hier siehst du deine gespeicheren Events, {{ Auth::user()->name }}.</p>
			</div>
		</div>

        <div class="row">
            <div class="small-12 large-6 large-centered columns events">

                @each('partials.my_event', $events, 'event', 'partials.event_empty')

            </div>

        </div>

@endsection