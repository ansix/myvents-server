$(document).foundation();



$(document).ready(function(){

    // SET CSFR TOKEN
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
    });
    
    // MODAL
    $modal = $('#modal');


	// REDRAW MAP ON TAB CHANGE
	$('[data-tabs]').on('change.zf.tabs', function() {
  		map.invalidateSize(true)
	});

	// Foundation Datepicker
	if($("input[name='filter[from]']") && $("input[name='filter[to]']")){
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		var checkin = $("input[name='filter[from]']").fdatepicker({
			format: 'dd.mm.yyyy',
			language: 'de',
			onRender: function (date) {
				return date.valueOf() < now.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function (ev) {
			if (ev.date.valueOf() > checkout.date.valueOf()) {
				var newDate = new Date(ev.date)
				newDate.setDate(newDate.getDate() + 1);
				checkout.update(newDate);
			}
			checkin.hide();
			$("input[name='filter[to]']")[0].focus();
		}).data('datepicker');
		var checkout = $("input[name='filter[to]']").fdatepicker({
			format: 'dd.mm.yyyy',
			language: 'de',
			onRender: function (date) {
				return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function (ev) {
			checkout.hide();
		}).data('datepicker');
	}

    // HANDLE ACTIONS RATE, ADD AND SHARE
    $('.action').on('click', function(event){
        var action = $(this).data("action"),
            id = $(this).data("url").split('/').reverse()[0];

        if(action != 'add'){
            $.ajax('/events/' + id + '/load-' +action)
                .done(function(resp){
                    $modal.html(resp).foundation('open');
                });
        } else {
            event.preventDefault();
            var data = $(this).data();
            submitForm({
                action: "",
                url: data.url,
                title: data.title
            });
        }
    });

});


