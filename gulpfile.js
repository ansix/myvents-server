var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

	var bowerPath   = "resources/assets/components/";

    mix.sass('app.scss','public/css', {
    	includePaths: [
    		bowerPath + 'foundation-sites/scss',
    		bowerPath + 'font-awesome/scss',
            bowerPath + 'motion-ui/src',
            bowerPath + 'foundation-datepicker/css']
    	});

    mix.scripts([
        'vendor/jquery.min.js',
        '../components/foundation-sites/dist/foundation.js',
        '../components/foundation-datepicker/js/foundation-datepicker.js',
        '../components/foundation-datepicker/js/locales/foundation-datepicker.de.js',
        'app.js'
    ]);
});
